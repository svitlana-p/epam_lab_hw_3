const jwt = require("jsonwebtoken");
const { getUserByID } = require('../services/usersService');
const authMiddleware = async (req, res, next) => {
  const { authorization } = req.headers;

  if (!authorization) {
    return res
      .status(401)
      .json({ message: "Please, provide authorization header" });
  }

  const [, token] = authorization.split(" ");

  if (!token) {
    return res
      .status(401)
      .json({ message: "Please, include token to request" });
  }

  try {
    const tokenPayload = jwt.verify(token, process.env.SECRET_JWT_KEY);
    const user = await getUserByID(tokenPayload.userID)

  if (!user) {
    throw Error("User doesn't exist")
  }

    req.user = {
      _id: tokenPayload.userID,
      role: tokenPayload.role,
      email: tokenPayload.email,
      created_date: tokenPayload.created_date
    };
    next();
  } catch (err) {
    return res.status(401).json({ message: err.message });
  }
};

module.exports = {
  authMiddleware,
};
