const { truckJoiShema } = require('../models/Truck')
const {
  getAllTrucksByUserID,
  addUserTruck,
  getUserTruckByID,
  updateUserTruckByID,
  deleteUserTruckByID,
  assignTruckToUserByID,
  getAssignedTruckByUserID,
  unassignUserTruckByID
} = require('../services/trucksService')

const getAllTrucks = async (req, res) => {
  const userID = req.user._id

  const trucks = await getAllTrucksByUserID(userID)

  return res.status(200).json({ trucks })
}

const saveTruck = async (req, res) => {
  const validatedData = await truckJoiShema.validateAsync({
    created_by: req.user._id,
    status: 'IS',
    ...req.body
  })

  await addUserTruck(validatedData)

  return res.status(200).json({
    message: 'Truck created successfully'
  })
}

const getTruck = async (req, res) => {
  const userID = req.user._id
  const truckID = req.params.id

  const truck = await getUserTruckByID(userID, truckID)

  if (!truck) {
    throw Error('Truck not found')
  }

  return res.status(200).json({ truck })
}

const updateTruck = async (req, res) => {
  const truckID = req.params.id
  const truckType = req.body.type

  await truckJoiShema.extract('type').validateAsync(truckType)

  await updateUserTruckByID(truckID, truckType)

  return res.status(200).json({
    message: 'Truck details changed successfully'
  })
}

const deleteTruck = async (req, res) => {
  const truckID = req.params.id

  const truck = await deleteUserTruckByID(truckID)

  if (!truck) {
    throw Error('Truck not found')
  }

  return res.status(200).json({
    message: 'Truck deleted successfully'
  })
}

const assignTruck = async (req, res) => {
  const userID = req.user._id
  const truckID = req.params.id

  const currentAssignedTruck = await getAssignedTruckByUserID(userID)

  if (currentAssignedTruck) {
    await unassignUserTruckByID(currentAssignedTruck._id)
  }

  await assignTruckToUserByID(userID, truckID)

  return res.status(200).json({
    message: 'Truck assigned successfully'
  })
}

module.exports = {
  getAllTrucks,
  saveTruck,
  getTruck,
  updateTruck,
  deleteTruck,
  assignTruck
}

