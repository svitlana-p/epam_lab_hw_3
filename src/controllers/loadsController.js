const { default: mongoose } = require('mongoose');
const { loadJoiShema, Load } = require('../models/Load');
const { Truck } = require('../models/Truck');

const {
  getAllLoadsByUserID,
  addUserLoad,
  getUserLoadByID,
  updateUserLoadByID,
  deleteUserLoadByID,
  updateUserLoadStatus,
  assignTruckToLoad,
  getLoadParams,
  getLoadsShippingInfo,
  compareTruckAndLoadParams
} = require('../services/loadsService');
const {
  getAssignedTrucks,
  getTruckParams
} = require('../services/trucksService')

const getAllLoads = async (req, res) => {
  const userRole = req.user.role
  const userID = req.user._id

  let loads = null

  if (userRole === 'DRIVER') {
    const assignedTruck = await Truck.findOne({
      assigned_to: new mongoose.mongo.ObjectId(userID)
    })

    const assignedTruckID = assignedTruck._id

    loads = await Load.findOne(
      {
        assigned_to: new mongoose.mongo.ObjectId(assignedTruckID),
        status: 'ASSIGNED'
      },
      '-__v'
    )
  } else {
    loads = await getAllLoadsByUserID(userID)
  }

  // const offset = +req.query.offset
  // const limit = +req.query.limit

  return res.status(200).json({ loads })
  //   res.status(200).json({
  //     offset: offset || 0,
  //     limit: limit || 0,
  //     count: notes.length,
  //     notes: notes.slice(offset, limit || notes.length)
  //   })
}

const addLoad = async (req, res) => {
  const validatedData = await loadJoiShema.validateAsync({
    created_by: req.user._id,
    ...req.body
  })

  await addUserLoad(validatedData)

  return res.status(200).json({
    message: 'Load created successfully'
  })
}

const getActiveLoad = async (req, res) => {
  const userID = req.user._id

  const assignedTruck = await Truck.findOne({
    assigned_to: new mongoose.mongo.ObjectId(userID),
    status: 'OL'
  })

  const assignedTruckID = assignedTruck._id

  const load = await Load.findOne(
    {
      assigned_to: new mongoose.mongo.ObjectId(assignedTruckID),
      status: 'ASSIGNED'
    },
    '-__v'
  )

  if (!load) {
    throw Error('No active load')
  }

  return res.status(200).json({ load })
}

const IterateNextLoadState = async (req, res) => {
  const states = [
    'En route to Pick Up',
    'Arrived to Pick Up',
    'En route to delivery',
    'Arrived to delivery'
  ]

  const userID = req.user._id

  const assignedTruck = await Truck.findOne({
    assigned_to: new mongoose.mongo.ObjectId(userID),
    status: 'OL'
  })

  const assignedTruckID = assignedTruck._id

  const load = await Load.findOne(
    {
      assigned_to: new mongoose.mongo.ObjectId(assignedTruckID),
      status: 'ASSIGNED'
    },
    '-__v'
  )

  if (!load) {
    throw Error('No active load')
  }

  const currentStateIndex = states.findIndex(
    (state) => state === load.state
  )

  if (currentStateIndex === 3) {
    throw Error('Load has already delivered')
  }

  const nextState = states[currentStateIndex + 1]

  if (nextState === 'Arrived to delivery') {
    await Load.findByIdAndUpdate(load._id, {
      state: nextState,
      status: 'SHIPPED',
      $push: {
        logs: {
          message: `Load state changed to '${nextState}'`,
          time: new Date()
        }
      }
    })
  }
  await Load.findByIdAndUpdate(load._id, {
    state: nextState,
    $push: {
      logs: {
        message: `Load state changed to '${nextState}'`,
        time: new Date()
      }
    }
  })

  return res.status(200).json({
    message: `Load state changed to '${nextState}'`
  })
}

const getLoad = async (req, res) => {
  const loadID = req.params.id

  const load = await getUserLoadByID(loadID)

  if (!load) {
    throw Error('Load not found')
  }

  return res.status(200).json({ load })
}

const updateLoad = async (req, res) => {
  const loadID = req.params.id

  await loadJoiShema.validateAsync(req.body)

  await updateUserLoadByID(loadID, req.body)

  return res.status(200).json({
    message: 'Load details changed successfully'
  })
}

const deleteLoad = async (req, res) => {
  const loadID = req.params.id

  const load = await deleteUserLoadByID(loadID)

  if (!load) {
    throw Error('Load not found')
  }

  res.status(200).json({
    message: 'Load deleted successfully'
  })
}

const postLoad = async (req, res) => {
  const loadID = req.params.id

  const load = await updateUserLoadStatus(loadID, 'POSTED')

  if (!load) {
    throw Error('Load not found')
  }

  const assignedTrucks = await getAssignedTrucks()

  if (!assignedTrucks.length) {
    throw Error('No available trucks found')
  }

  const truckID = assignedTrucks[0]._id
  const driverID = assignedTrucks[0].assigned_to

  await assignTruckToLoad(loadID, truckID, driverID)

  await updateUserLoadStatus(loadID, 'ASSIGNED')

  const loadParams = await getLoadParams(loadID)

  const truckParams = await getTruckParams(truckID)

  compareTruckAndLoadParams(truckParams, loadParams)

  return res.status(200).json({
    message: 'Load posted successfully',
    driver_found: true
  })
}

const getLoadsInfo = async (req, res) => {
  const userID = req.user._id

  const loads = await getLoadsShippingInfo(userID)

  if (!loads.length) {
    throw Error('Loads not found')
  }

  return res.status(200).json({ loads })
}

module.exports = {
  getAllLoads,
  addLoad,
  getActiveLoad,
  IterateNextLoadState,
  getLoad,
  updateLoad,
  deleteLoad,
  postLoad,
  getLoadsInfo
}
