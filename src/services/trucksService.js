const mongoose = require('mongoose')
const { Truck } = require('../models/Truck')

const getAllTrucksByUserID = async (id) => {
  return await Truck.find({ created_by: id }, '-__v')
}

const addUserTruck = async (data) => {
  const truck = new Truck(data)

  return await truck.save()
}

const getUserTruckByID = async (truckID) => {
  return await Truck.findById(truckID, '-__v')
}

const updateUserTruckByID = async (truckID, type) => {
  return await Truck.findByIdAndUpdate(truckID, { type })
}

const deleteUserTruckByID = async (truckID) => {
  return await Truck.findByIdAndDelete(truckID)
}

const assignTruckToUserByID = async (userID, truckID) => {
  return await Truck.findByIdAndUpdate(truckID, {
    assigned_to: new mongoose.mongo.ObjectId(userID)
  })
}

const unassignUserTruckByID = async (truckID) => {
  return await Truck.findByIdAndUpdate(truckID, { assigned_to: null })
}

const getAssignedTruckByUserID = async (userID) => {
  return await Truck.findOne({
    assigned_to: new mongoose.mongo.ObjectId(userID)
  })
}

const getAssignedTrucks = async () => {
  return await Truck.aggregate([
    {
      $match: {
        status: 'IS',
        assigned_to: {
          $ne: null
        }
      }
    }
  ])
}

const getTruckParams = async (truckID) => {
  const { payload, dimensions } = await Truck.findById(truckID)

  return { payload, dimensions }
}

module.exports = {
  getAllTrucksByUserID,
  addUserTruck,
  getUserTruckByID,
  updateUserTruckByID,
  deleteUserTruckByID,
  assignTruckToUserByID,
  unassignUserTruckByID,
  getAssignedTruckByUserID,
  getAssignedTrucks,
  getTruckParams
}
