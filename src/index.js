
const express = require('express');
const cors = require('cors');
const morgan = require('morgan');
const mongoose = require('mongoose');
const dotenv = require('dotenv');
dotenv.config();

const { authRouter } = require('./routers/authRouter');
const { usersRouter } = require('./routers/usersRouter');
const { trucksRouter } = require('./routers/trucksRouter');
const { loadsRouter } = require('./routers/loadsRouter');

const { asyncWrapper } = require('./asyncWrapper');
const { authMiddleware } = require('./middlewares/authMiddleware');
const {
    driverMiddleware
  } = require('./middlewares/driverMiddleware');
  

const app = express();

app.use(cors());
app.use(express.json());
app.use(morgan('tiny'));
app.use(express.urlencoded({ extended: false }));

app.use('/api/auth', authRouter)
app.use('/api/users/me', asyncWrapper(authMiddleware), usersRouter)
app.use('/api/loads', asyncWrapper(authMiddleware), loadsRouter)
app.use(
  '/api/trucks',
  asyncWrapper(authMiddleware),
  asyncWrapper(driverMiddleware),
  trucksRouter
)




app.use(errorHandler);

const PORT = process.env.PORT || 8080;
const DB_USER = process.env.DB_USER;
const DB_PASSWORD = process.env.DB_PASSWORD;

async function start() {
    try {
        await mongoose.connect(
            `mongodb+srv://${DB_USER}:${DB_PASSWORD}@cluster0.22vwbrd.mongodb.net/hw3?retryWrites=true&w=majority`
        );
        app.listen(PORT, () => console.log(`Server started at port: ${PORT}`))
    } catch (err) {
        console.log(err);
    }
}

start();



function errorHandler(err, req, res, next) {
  return res.status(400).json({ message: err.message })
}
