const express = require('express')
const router = express.Router()
const { getUserInfo, deleteUser, updatePassword } = require('../controllers/usersController');
const { passwordMiddleware } = require('../middlewares/passwordMiddleware');
const { asyncWrapper } = require('../asyncWrapper');

router.get('/', asyncWrapper(getUserInfo));

router.delete('/', asyncWrapper(deleteUser));

router.patch('/password', asyncWrapper(passwordMiddleware),
  asyncWrapper(updatePassword));

module.exports = {
  usersRouter: router
}
