const express = require('express');
const router = express.Router();

const {
  getAllLoads,
  addLoad,
  getActiveLoad,
  IterateNextLoadState,
  getLoad,
  updateLoad,
  deleteLoad,
  postLoad,
  getLoadsInfo
} = require('../controllers/loadsController')

const { shipperMiddleware } = require('../middlewares/shipperMiddleware');
const { driverMiddleware } = require('../middlewares/driverMiddleware');
const { asyncWrapper } = require('../asyncWrapper');

router.get('/', asyncWrapper(getAllLoads));

router.post('/', asyncWrapper(shipperMiddleware),
  asyncWrapper(addLoad));

router.get('/active', asyncWrapper(driverMiddleware),
  asyncWrapper(getActiveLoad));

router.patch('/active/state', asyncWrapper(driverMiddleware),
  asyncWrapper(IterateNextLoadState));

router.get('/:id', asyncWrapper(getLoad));

router.put('/:id', asyncWrapper(shipperMiddleware),
  asyncWrapper(updateLoad));

router.delete('/:id', asyncWrapper(shipperMiddleware),
  asyncWrapper(deleteLoad));

router.post('/:id/post', asyncWrapper(shipperMiddleware),
  asyncWrapper(postLoad));

router.get('/:id/shipping_info', asyncWrapper(shipperMiddleware),
  asyncWrapper(getLoadsInfo));

module.exports = {
  loadsRouter: router
}
