const express = require('express');
const router = express.Router();

const {
  getAllTrucks,
  saveTruck,
  getTruck,
  updateTruck,
  deleteTruck,
  assignTruck
} = require('../controllers/trucksController');

const { asyncWrapper } = require('../asyncWrapper');

router.get('/', asyncWrapper(getAllTrucks));

router.post('/', asyncWrapper(saveTruck));

router.get('/:id', asyncWrapper(getTruck));

router.put('/:id', asyncWrapper(updateTruck));

router.delete('/:id', asyncWrapper(deleteTruck));

router.post('/:id/assign', assignTruck);

module.exports = {
  trucksRouter: router
}
